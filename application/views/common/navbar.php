<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Controle Financeiro</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" 
  data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
  aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('home') ?>">Home 
        <span class="sr-only">(current)</span>
        </a>
      </li>


      <!--DropDown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a>
          <a class="dropdown-item" href="#">Conta Bancária</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Parceiros</a>
        </div>

     <!--DropDown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lançamentos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url('contas/pagar')?>">Contas a Pagar</a>
          <a class="dropdown-item" href="<?= base_url('contas/receber')?>">Contas a Receber</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Fluxo de caixa</a>
        </div>



        <!--DropDown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Relatórios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Lançamentos por Período</a>
          <a class="dropdown-item" href="<?= base_url('contas/movimento')?>">Movimento de Caixa</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Resumo Anual</a>
        </div>
    </ul>

  </div>
</nav>