<div class="container">
      <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
        <div class="text-center">

            <h3 class="mb-5">Controle Financeiro Pessoal</h3>

                <form class="text-center border border-light p-5" method="POST">
                    <p class="h4 mb-4">Entrar </p>
                    <div class="form-outline mb-4">
                        <input type="email" id="email" name="email" class="form-control" />
                        <label class="form-label" for="email">Email</label>
                    </div>

                    <div class="form-outline mb-4">
                        <input type="password" id="senha" name="senha" class="form-control" />
                        <label class="form-label" for="senha">Senha</label>
                    </div>

                    <div class="row mb-4">
                        <div class="col d-flex justify-content-center">
                        <div class="form-check">
                            <input
                            class="form-check-input"
                            type="checkbox"
                            value=""
                            id="form2Example3"
                            checked
                            />
                            <label class="form-check-label" for="form2Example3"> Lembrar-me </label>
                        </div>
                        </div>

                        <div class="col">
                            <a href="#!">Esqueceu a senha?</a>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block mb-4">Login</button>
                <p class="red-text"> <?= $error ? 'Dados de acesso incorretos ' : '' ?> </p>
                </form>
        </div>
    </div>
</div>