<?
defined('BASEPATH') OR exit('No direct script access allowed');
require APPATH . '/libraries/rest/MyRestController.php';

class SampleRest extends MyRestController{

    function __construct(){
        parent::__construct ('sample');
    }

    function action one_post(){
        $res = $this->model->action_one();
        $this->response($res, RESTControler::HTTP_OK);
    }

    function action_two_get(){
        $res = $this->model->action_two();
        $this->response($res, RESTControler::HTTP_OK);
    }
}


?>